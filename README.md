# README 

The files included in this repo are to help get you started.  Consider them a template or starting point from which you can use some or all of the files with your Ruby application.

You will find three different docker-compose files:

 - docker-compose.yml (very basic, typically used for running test suite)
 - docker-compose-dev.yml (this version used for your development environment)
 - docker-compose-dev-prom-graf.yml (advanced, used for dev environment with prometheus and grafana support)

By default the docker-compose command will look for a file called docker-compose.yml in the current directory.  You can specify a different file using the -f flag.  For example:

```shell
docker-compose up  <-- uses the default docker-compose.yml file in the current directory

docker-compose -f docker-compose-dev.yml up  <-- use a file called docker-compose-dev.yml instead of the default docker-compose.yml

# NOTE:  use the --build flag to ensure docker builds a fresh image using a Dockerfile found in the current directory.
```

Also included with this repo are two Docker files:

 - Dockerfile (used with docker-compose, copies your apps file into the image it creates)
 - Dockerfile_Ruby264 (used for spinning up a dev environment, outside of using docker-compose)

The final file to note is the .gitlab-ci.yml.  This file is used to support the Auto DevOps CI/CD functionality at Gitlab.

The included file, when executed, will cycle through different stages including:

 - Init
 - Lint(ing)
 - Test(ing)
 - Deploy

In this provided .gitlab-ci.yml file we are executing three different test scenarios, and these will run in parallel on Gitlab.com.  If you are running your own GitLab server they can run in parallel if you have enough runners configured.

For the deploy stage in this example, we run through steps to package our app as an RPM (RedHat/CentOS/Fedora/OracleLinux.)  You can swap these instructions out with ones that do a deploy, for example to Heroku or Gigalixir.  For more information on using GitLab to deploy to Heroku/Gigalixir, see the repo [metademo_jaxex](https://gitlab.com/jhogarty/metademo_jaxex)
 
