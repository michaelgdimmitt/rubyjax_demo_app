#! /bin/bash

# Preparation and execution of database migrations 
mkdir logs/ 
touch logs/test_db_query.log 
touch logs/test_access.log 
touch logs/test_error.log

# Run database migrations
# ./db/migrate.rb
