FROM ruby:2.6.4-stretch
MAINTAINER John F. Hogarty <hogihung@gmail.com>

RUN apt-get update -qq && apt-get install -y build-essential mysql-client netcat

RUN mkdir /demo-app
WORKDIR /demo-app

COPY Gemfile Gemfile.lock ./
RUN gem install bundler && bundle install --binstubs --jobs 20 --retry 5

# Copy over our application
COPY . .

EXPOSE 9070 
EXPOSE 9080 
EXPOSE 9304

ENTRYPOINT ["sh", ".docker/startup.sh"]

