#!/bin/bash

DEMOAPPFILE=$1
DEPLOY_DIR='/usr/deploy'
RPMBUILD_V6='/root/rpmbuild'

function get_os_version() {
  RAW_VERSION=$(cat /etc/redhat-release)
  VERSION=$(echo "${RAW_VERSION//[!0-9]/}" | cut -c 1)
  echo $VERSION
}
CURROS=$(get_os_version)

if [ "$(echo $CURROS)" = "6" ]
then
  RPM_SOURCE=$RPMBUILD_V6
else
  echo "Unknown release, exitting"
  exit
fi

if [ ! -d $RPM_SOURCE/ ]
then
  echo "RPM Build tool not yet initialized, will initialize to build needed directories."
  rpmbuild -bb $RPM_SOURCE/SPECS/demo-app.spec > /dev/null 2>&1
fi

if [ ! -d $DEPLOY_DIR ]
then
  echo "Directory $DEPLOY_DIR does not exist. Creating required directory"
  mkdir -p $DEPLOY_DIR
fi

if [ -z "$DEMOAPPFILE" ]
then
  echo "Missing required file name for tar file, exiting."
  echo "Example:  ./cicd-create-rpm.sh demo-app-0.1.2"
  exit 1
else
  echo "Attempting to build Demo App RPM using the name: $DEMOAPPFILE"
fi

echo "Preparing to create a new RPM for Demo App"
echo "........................................."

echo "Copying the demo-app directory to /usr/deploy"
echo "........................................."
rm -rf $DEPLOY_DIR/demo-app
cp -rp $WORKDIR/ $DEPLOY_DIR

echo "Copying the demo-app.spec file to the RPM Builder directory"
echo "........................................."
cp $WORKDIR/dist/demo-app.spec $RPM_SOURCE/SPECS

echo "Cleaning up deploy directory"
echo "........................................."
cd $DEPLOY_DIR/demo-app
rm -rf bin .bundle dev dist docs .DS_Store external .git .gitignore README.md RPMS .ruby-version test vendor demo-app*.tar Dockerfile* docker-compose.yml logs/*.log .docker/
mkdir -p bin
cp $WORKDIR/bin/console bin/console

# If you need a modified version of the Gemfile and/or Gemfile.lock when building
# a RPM, uncomment below lines and ensure your *_RPMBldr files exist
# echo "Splice in Gemfile and Gemfile.lock files needed for RPM Builder"
# cp Gemfile_RPMBldr Gemfile
# cp Gemfile_lock_RPMBldr Gemfile.lock

echo "Running bundler to pull in gem dependencies"
echo "........................................."
bundle install --deployment --binstubs --without test --without development

echo "Archiving project directory"
echo "........................................."
tar -czvf $DEMOAPPFILE.tar.gz .
mv $DEMOAPPFILE.tar.gz $RPM_SOURCE/SOURCES

echo "Building the Demo App RPM"
echo "........................................."
rpmbuild -bb $RPM_SOURCE/SPECS/demo-app.spec

echo "Moving build RPM file"
echo "........................................."
mv /root/rpmbuild/RPMS/x86_64/demo-app*.rpm $WORKDIR/

